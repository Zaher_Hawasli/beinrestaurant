import { createWebHistory, createRouter } from "vue-router";
const Welcome = () => import('@/components/Welcome.vue' /* webpackChunkName: "resource/js/components/welcome" */)

// App
const App = () => import('@/components/App.vue')
// App


const CategoryList = () => import('@/components/category/List.vue' /* webpackChunkName: "resource/js/components/category/list" */)
const CategoryCreate = () => import('@/components/category/Add.vue' /* webpackChunkName: "resource/js/components/category/add" */)
const CategoryEdit = () => import('@/components/category/Edit.vue' /* webpackChunkName: "resource/js/components/category/edit" */)

const SubcategoryList = () => import('@/components/Subcategory/List.vue' /* webpackChunkName: "resource/js/components/Subcategory/list" */)
const SubcategoryCreate = () => import('@/components/Subcategory/Add.vue' /* webpackChunkName: "resource/js/components/Subcategory/add" */)
const SubcategoryEdit = () => import('@/components/Subcategory/Edit.vue' /* webpackChunkName: "resource/js/components/Subcategory/edit" */)

const ItemList = () => import('@/components/Item/List.vue' /* webpackChunkName: "resource/js/components/Item/list" */)
const ItemCreate = () => import('@/components/Item/Add.vue' /* webpackChunkName: "resource/js/components/Item/add" */)
const ItemEdit = () => import('@/components/Item/Edit.vue' /* webpackChunkName: "resource/js/components/Item/edit" */)

const DiscountList = () => import('@/components/Discount/List.vue' /* webpackChunkName: "resource/js/components/Discount/list" */)
const DiscountCreate = () => import('@/components/Discount/Add.vue' /* webpackChunkName: "resource/js/components/Discount/add" */)

const menu = () => import('@/components/Menu.vue')


export const routes = [
    {
        name: 'home',
        path: '/',
        component: Welcome
    },

    {
        name: 'app',
        path: '/app',
        component: App
    },


    {
        name: 'categoryList',
        path: '/category',
        component: CategoryList
    },
    {
        name: 'categoryEdit',
        path: '/category/:id/edit',
        component: CategoryEdit
    },
    {
        name: 'categoryAdd',
        path: '/category/add',
        component: CategoryCreate
    }
    ,


    {
        name: 'SubcategoryList',
        path: '/Subcategory',
        component: SubcategoryList
    },
    {
        name: 'SubcategoryEdit',
        path: '/Subcategory/:id/edit',
        component: SubcategoryEdit
    },
    {
        name: 'SubcategoryAdd',
        path: '/Subcategory/add',
        component: SubcategoryCreate
    }
    ,


    {
        name: 'ItemList',
        path: '/item',
        component: ItemList
    },
    {
        name: 'ItemEdit',
        path: '/item/:id/edit',
        component: ItemEdit
    },
    {
        name: 'ItemAdd',
        path: '/item/add',
        component: ItemCreate
    }
    ,


    {
        name: 'DiscountList',
        path: '/discount',
        component: DiscountList
    },
    {
        name: 'DiscountAdd',
        path: '/discount/add',
        component: DiscountCreate
    }


];

const router = createRouter({
    history: createWebHistory(),
    routes,
  });

  

  export default router;