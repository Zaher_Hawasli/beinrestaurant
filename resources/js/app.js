

import './bootstrap';

import '../sass/app.scss'
import { createApp } from 'vue';
import VueAxios from 'vue-axios';
import axios from 'axios';

import router from '@/router'

// import $ from 'jquery';
// window.$ = $;

// Vue.prototype.jQuery = jQuery

// import jQuery from 'jquery'

// window.jQuery = jQuery



// import Select2Component


import Select2 from 'vue3-select2-component';



const app = createApp({});

// Define a new global component called button-counter
app.component('Select2', Select2);

app.use(router);
app.use(VueAxios, axios);

app.mount('#app');
