<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\DiscountController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// catgeory
Route::resource('category',CategoryController::class)->except(['create', 'edit']);
Route::get('subcategory',[CategoryController::class,'SubcategoriesIndex']);
Route::get('get_menu',[CategoryController::class,'getMenu']);
Route::get('getSubcategories/{id}',[CategoryController::class,'SelectSubcategories']);



// item
Route::resource('item',ItemController::class)->except(['create', 'edit']);
Route::get('create_item_category',[ItemController::class,'ItemCreateCategory']);
Route::get('create_item_subcategory/{id}',[ItemController::class,'ItemCreateSubCategories']);


// discount
Route::resource('discount',DiscountController::class)->except(['create', 'edit']);

