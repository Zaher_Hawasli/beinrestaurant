<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $guarded=[];
    protected $with=['Children.Discount'];


    /**
     * Get the Category that owns the Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Category()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }
    public function Item()
    {
        return $this->hasMany(Item::class);
    }

    
    public function Discount()
    {
        return $this->hasOne(Discount::class)->latest();
    }

    public function Children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

}
