<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    use HasFactory;
    protected $guarded=['parent_id'];
    protected $appends=['discount_type','model_name'];


    /**
     * Get the Category that owns the Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Category()
    {
        return $this->belongsTo(Category::class);
    }
    /**
     * Get the Category that owns the Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Item()
    {
        return $this->belongsTo(Item::class);
    }

    public function getDiscountTypeAttribute()
    {               
        $types=[
            0=>'All menu',
            1=>'Category',
            2=>'Subcategory',
            3=>'Item'
        ];
        return $types[$this->type];
    }

    public function getModelNameAttribute()
    {               
        $model=$this->Category??$this->Item;
        return $model?$model->name:'';
    }


    
}
