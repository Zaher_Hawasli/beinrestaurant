<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $guarded=[];
    // protected $appends=['item_discount'];


    /**
     * Get the Category that owns the Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Category()
    {
        return $this->belongsTo(Category::class);
    }

    public function Discount()
    {
        return $this->hasOne(Discount::class)->latest();
    }

    
    // public function getItemDiscountAttribute()
    // {               
    //     $discount=$this->Discount?$this->Discount->amount:'';
    //     if($discount==''){
    //         $item_category=$this->Category;
    //     $discount=$item_category->Discount?$item_category->Discount->amount:'';

    //     while(isset($item_category)&&(!isset($discount))){
    //         $item_category=$item_category->Category;
    //         $discount=$item_category->Discount?$item_category->Discount->amount:'';
   
    //        }

    //     }
    //     return $discount;

    // }
}
